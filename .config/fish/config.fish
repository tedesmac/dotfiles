# Adds ~/bin to PATH
set PATH $PATH $HOME/.local/bin/


# Sets XDG_DATA_DIR as it is not read correctly by fish
set -l xdg_data_home $XDG_DATA_HOME ~/.local/share
set -gx --path XDG_DATA_DIRS $xdg_data_home[1]/flatpak/exports/share:/var/lib/flatpak/exports/share:/usr/local/share:/usr/share

for flatpakdir in ~/.local/share/flatpak/exports/bin /var/lib/flatpak/exports/bin
    if test -d $flatpakdir
        contains $flatpakdir $PATH; or set -a PATH $flatpakdir
    end
end


# git but for dot files
alias dot='/usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME'
alias dot-commit='dot commit -am'
alias dot-ls='dot ls-tree -r HEAD --name-only'
alias dot-remove='dot rm --cached'


# Loads terminal colors using wal
# set wal_path (whereis wal)
# if not test "$wal-path" = ""
#     wal -n -R -q
# end
