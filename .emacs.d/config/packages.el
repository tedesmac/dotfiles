;; IDO
(require 'ido)
(ido-mode t)

;; Org mode
(require 'org)
(define-key global-map "\C-cl" 'org-store-link)
(define-key global-map "\C-ca" 'org-agenda)
(setq org-log-done t)

;; Melpa
(require 'package)
(setq package-enable-at-startup nil)
(add-to-list 'package-archives
	         '("melpa" . "https://melpa.org/packages/"))
;;(add-to-list
(package-initialize)

(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

;; Srcery Theme
(use-package doom-themes
  :ensure t
  :config
 (load-theme 'doom-horizon t))

;; Column enforce mode
(use-package column-enforce-mode
  :ensure t
  :config
  (setq column-enforce-column 80)
  (global-column-enforce-mode))

;; Evil
(use-package evil
  :ensure t
  :config
  (evil-mode t)
  (evil-ex-define-cmd "q" 'kill-this-buffer)
  )

;; general.el
(use-package general
  :ensure t)

;; Ivy
(use-package ivy
  :ensure t
  :commands (avy-goto-word-1)
  :config
  (use-package counsel
    :ensure t
    :config
    (counsel-mode)))

;; Magit
(use-package magit
  :defer t)

;; Super save
(use-package super-save
  :ensure t
  :config
  (super-save-mode +1))

;; Wakatime
(use-package wakatime-mode
  :ensure t
  :config
  (global-wakatime-mode t)
  (setq wakatime-cli-path "$HOME/.local/bin/wakatime")
  )

;; Which Key
(use-package which-key
  :ensure t
  :config
  (which-key-mode)
  (setq which-key-idle-delay 0.1)
  )

;; Spell Check
(use-package wucuo
  :ensure t
  :config
  (wucuo-start)
  (setq ispell-extra-args "--run-together"))

;;
;; Programming Language configuration
;;

(use-package company
  :ensure t
  :config
  (add-hook 'after-init-hook 'global-company-mode)
  (setq company-idle-delay 0.2)
  )

(use-package company-lsp
  :ensure t
  )

(use-package hl-todo
  :ensure t
  :config
  (setq hl-todo-keyword-faces
        '(("TODO" . "#FF0000")
          ("FIXME" . "#FF0000")
          ("DEBUG" . "#FFFF00")))
  (global-hl-todo-mode))

(use-package lsp-mode
  :ensure t
  )

(use-package lsp-ui
  :ensure t
  :config
  (add-hook 'lsp-mode-hook 'lsp-ui-mode)
  (setq lsp-ui-sideline-enable nil)
  (setq lsp-ui-doc-enable t)
  )

(use-package origami
 :ensure t
 :config
 (global-origami-mode)
 )

(use-package prettier-js
  :ensure t
  )

(use-package projectile
  :ensure t
  :config
  (projectile-mode +1)
  (setq
   projectile-project-search-path
   '("~/Development/")
   )
  )

(use-package ripgrep
  :ensure t
  )

;; Languages

;; CSS
;; vscode-css-languageserver-bin required
;; $> npm install -g vscode-css-languageserver-bin
(add-hook 'css-mode #'lsp)
(setq css-indent-offset 2)

(add-hook 'css-mode-hook 'prettier-js-mode)

;; HTML
;; vscode-html-languageserver required
;; $> npm install -g vscode-html-languageserver-bin
(add-hook 'html-mode-hook #'lsp)
(add-hook 'html-mode-hook 'prettier-js-mode)

;; Javascript
;; typescript-language-server required
;; $> npm install -g typescript-language-server
(setq js-indent-level 2)
(add-hook 'js-mode-hook 'prettier-js-mode)
;; (add-hook 'js-mode-hook #'lsp)
(use-package js2-mode
  :defer t
  :config
  (add-to-list 'auto-mode-alist '("\\.js\\'" . js2-mode))
  (add-hook 'js2-mode-hook #'prettier-js-mode)
  ;; (add-hook 'js2-mode-hook #'lsp)
  )

(defun setup-tide-mode ()
  (interactive)
  (tide-setup)
  (flycheck-mode +1)
  (setq flycheck-check-syntax-automatically '(save mode-enabled))
  (eldoc-mode +1)
  (tide-hl-identifier-mode +1)
  (company-mode +1)
  )

(setq company-tooltip-align-annotations t)

(use-package tide
  :defer t
  :config
  (add-hook 'before-save-hook 'tide-format-before-save)
  (add-hook 'js2-mode-hook #'setup-tide-mode)
  (flycheck-add-next-checker 'javascript-eslint 'javascript-tide 'append)
  (setq js2-strict-missing-semi-warning nil)
  )


;; LaTeX
;; auctex is required
;; $> pacman -S auctex
(defun flymake-get-tex-args (file-name)
  (list "pdflatex"
        (list "-file-line-error" "-draftmode" "-interaction=nonstop" file-name)
        )
  )

(use-package auctex
  :defer t
  :hook (LaTeX-mode . flymake)
  :config

  ;; Enables parsing
  (setq TeX-auto-save t)
  (setq TeX-parse-self t)

  ;; Allows multi-file documents
  (setq-default TeX-master nil)
  )

(use-package company-auctex
  :defer t
  :config
  (company-auctex-init)
  )

;; Python
;; microsoft-python-language-server must be installed
(setq python-indent-offset 4)

(use-package lsp-python-ms
  :defer t
  :hook (python-mode . (lambda ()
                         (require 'lsp-python-ms)
                         (lsp)))
  )

(use-package python-black
  :demand t
  :after python-mode
  )

;; Rust
;; rls must be installed
;; $> rustup component add rls rust-analysis rust-src
(use-package rust-mode
  :defer t
  :hook (rust-mode . lsp)
  )

;; Typescript
(use-package typescript-mode
  :defer t
  :config
  (add-hook 'typescript-mode-hook 'prettier-js-mode)
  (add-hook 'typescript-mode-hook 'lsp-mode)
  (setq typescript-indent-level 2)
  )

;; Vue
(use-package vue-mode
  :defer t
  ; :hook (vue-mode . lsp)
  :config
  (add-hook 'mmm-mode-hook
            (lambda ()
              (set-face-background 'mmm-default-submode-face nil)
              )
            )
  (add-hook 'vue-mode-hook #'prettier-js-mode)
  )

;; Web mode
(use-package web-mode
  :defer t
  :config
  (add-to-list 'auto-mode-alist '("\\.php\\'" . web-mode))
  )
