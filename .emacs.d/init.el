;; straight.el
(setq straight-use-package-by-default t)

(defvar bootstrap-version)
(let ((bootstrap-file
       (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
      (bootstrap-version 5))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
        (url-retrieve-synchronously
         "https://raw.githubusercontent.com/raxod502/straight.el/develop/install.el"
         'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage));; load packages
(load "~/.emacs.d/config/packages.el")

(straight-use-package 'use-package)
(setq straight-use-package-by-default t)

;; ivy
(use-package ivy)

;; load custom functions
(load "~/.emacs.d/config/functions.el")

;; load base config
(load "~/.emacs.d/config/base.el")

;; load key bindings
(load "~/.emacs.d/config/keys.el")

;; =============================================================================
;; Auto generated code
;; =============================================================================

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   '("56911bd75304fdb19619c9cb4c7b0511214d93f18e566e5b954416756a20cc80" default))
 '(package-selected-packages
   '(go-mode svelte-mode php-mode doom-themes origami hl-todo lsp-python-ms js2-mode tide typescript-mode wakatime-mode company-auctex auctex evil rust-mode vue-mode lsp-vue company-lsp lsp-ui lsp-javascript-flow markdown-mode projectile fish-mode lua-mode super-save flycheck keyfreq counsel avy general which-key wucuo use-package sublimity srcery-theme prettier-js magit column-enforce-mode)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
